public class MediaController extends MediaListController {

	private MediaData media;
	private NewLabelScreen screen;

	public MediaController (MainUIMidlet midlet, AlbumData albumData, AlbumListScreen albumListScreen) {
		super(midlet, albumData, albumListScreen);
	}

	public boolean handleCommand(Command command) {
		String label = command.getLabel();
		System.out.println( "<* MediaController.handleCommand() *> " + label);
		
		// &begin [includeAccessControl]
		if (label.equals("Delete Label")) {
			if (!AccessController.hasDeleteRights()) {
				gotoAccessDeniedScreen();
				return true;
			}
		}

		if (label.equals("Add") || label.equals("Edit Label")) {
			if (!AccessController.hasWriteRights()) {
				gotoAccessDeniedScreen();
				return true;
			}
		}
		// &end [includeAccessControl]
		
		if (label.equals("Add")) {
			ScreenSingleton.getInstance().setCurrentScreenName(Constants.ADDPHOTOTOALBUM_SCREEN);
			AddMediaToAlbum form = new AddMediaToAlbum("Add new item to Album");
			form.setCommandListener(this);
			setCurrentScreen(form);
			return true;
		
		}
		// &begin [includePhoto]
		else if (label.equals("View")) {
			String selectedImageName = getSelectedMediaName();
			showImage(selectedImageName);
			// &begin [includeSorting]
			incrementCountViews(selectedImageName);
			// &end [includeSorting]
			ScreenSingleton.getInstance().setCurrentScreenName(Constants.IMAGE_SCREEN);
			return true;
		}
		// &end [includePhoto]
		
		// &begin [includeMusic]
		else if (label.equals("Play")) {
				String selectedMediaName = getSelectedMediaName();
				return playMusicMedia(selectedMediaName);		
		} 
		// &end [includeMusic]
		
		// &begin [includeVideo]
		else if (label.equals("Play Video")) {
				String selectedMediaName = getSelectedMediaName();
				return playVideoMedia(selectedMediaName);		
		}
		// &end [includeVideo]
		
		// &begin [captureVideo]
		else if (label.equals("Capture Video")) {
			
			CaptureVideoScreen playscree = new CaptureVideoScreen(midlet, CaptureVideoScreen.CAPTUREVIDEO);
			playscree.setVisibleVideo();
			VideoCaptureController controller = new VideoCaptureController(midlet, getAlbumData(), (AlbumListScreen) getAlbumListScreen(), playscree);
			this.setNextController(controller);
			playscree.setCommandListener(this);
			return true;		
		}
		// &end [captureVideo]

		// &begin [capturePhoto]
		else if (label.equals("Capture Photo")) {
			CaptureVideoScreen playscree = new CaptureVideoScreen(midlet, CaptureVideoScreen.CAPTUREPHOTO);
			playscree.setVisibleVideo();
			PhotoViewController controller = new PhotoViewController(midlet, getAlbumData(), (AlbumListScreen) getAlbumListScreen(), "New photo");
			controller.setCpVideoScreen(playscree);
			this.setNextController(controller);
			playscree.setCommandListener(this);
			return true;		
		}
		// &end [capturePhoto]
		
		else if (label.equals("Save Item")) {
			try {
				getAlbumData().addNewMediaToAlbum(((AddMediaToAlbum) getCurrentScreen()).getItemName(), 
						((AddMediaToAlbum) getCurrentScreen()).getPath(), getCurrentStoreName());
				// &begin [includeMusic]
				if (getAlbumData() instanceof MusicAlbumData){
					getAlbumData().loadMediaDataFromRMS(getCurrentStoreName());
					MediaData mymedia = getAlbumData().getMediaInfo(((AddMediaToAlbum) getCurrentScreen()).getItemName());
					mymedia.setTypeMedia( ((AddMediaToAlbum) getCurrentScreen()).getItemType() );
					getAlbumData().updateMediaInfo(mymedia, mymedia);
				}
				// &end [includeMusic]

			} catch (InvalidImageDataException e) {
				Alert alert = null;
				if (e instanceof ImagePathNotValidException)
					alert = new Alert("Error", "The path is not valid", null, AlertType.ERROR);
				else
					alert = new Alert("Error", "The file format is not valid", null, AlertType.ERROR);
				Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
				return true;
			} catch (PersistenceMechanismException e) {
				Alert alert = null;
				if (e.getCause() instanceof RecordStoreFullException)
					alert = new Alert("Error", "The mobile database is full", null, AlertType.ERROR);
				else
					alert = new Alert("Error", "The mobile database can not add a new photo", null, AlertType.ERROR);
				Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
			}
			// &begin [includeMusic]
			catch (ImageNotFoundException e) {
				Alert alert = new Alert("Error", "The selected item was not found in the mobile device", null, AlertType.ERROR);
				Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
				return true;
			}
			// &end [includeMusic]
			return goToPreviousScreen();
		} else if (label.equals("Delete")) {
			String selectedMediaName = getSelectedMediaName();
			try {
				getAlbumData().deleteMedia(getCurrentStoreName(), selectedMediaName);
			} catch (PersistenceMechanismException e) {
				Alert alert = new Alert("Error", "The mobile database can not delete this item", null, AlertType.ERROR);
				Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
				return true;
			} catch (ImageNotFoundException e) {
				Alert alert = new Alert("Error", "The selected item was not found in the mobile device", null, AlertType.ERROR);
				Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
				return true;
			}
			showMediaList(getCurrentStoreName(), false, false);
			ScreenSingleton.getInstance().setCurrentScreenName(Constants.IMAGELIST_SCREEN);
			return true;
		} else if (label.equals("Edit Label")) {
			String selectedImageName = getSelectedMediaName();
			try {
				media = getAlbumData().getMediaInfo(
						selectedImageName);
				NewLabelScreen formScreen = new NewLabelScreen(
						"Edit Label Item", NewLabelScreen.LABEL_PHOTO);
				formScreen.setCommandListener(this);
				this.setScreen(formScreen);
				setCurrentScreen(formScreen);
				formScreen = null;
			} catch (ImageNotFoundException e) {
				Alert alert = new Alert(
						"Error",
						"The selected item was not found in the mobile device",
						null, AlertType.ERROR);
				Display.getDisplay(midlet).setCurrent(alert,
						Display.getDisplay(midlet).getCurrent());
			}
			return true;

		// &begin [includeSorting]
		} else if (label.equals("Sort by Views")) {
			showMediaList(getCurrentStoreName(), true, false);
			ScreenSingleton.getInstance().setCurrentScreenName(Constants.IMAGELIST_SCREEN);

			return true;
		// &end [includeSorting]

		// &begin [includeFavourites]
		} else if (label.equals("Set Favorite")) {
			String selectedMediaName = getSelectedMediaName();
			try {
				MediaData media = getAlbumData().getMediaInfo(selectedMediaName);
				media.toggleFavorite();
				updateMedia(media);
				System.out.println("<* BaseController.handleCommand() *> Image = "+ selectedMediaName + "; Favorite = " + media.isFavorite());
			} catch (ImageNotFoundException e) {
				Alert alert = new Alert("Error", "The selected item was not found in the mobile device", null, AlertType.ERROR);
				Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
			} catch (InvalidImageDataException e) {
				Alert alert = null;
				if (e instanceof ImagePathNotValidException)
					alert = new Alert("Error", "The path is not valid", null, AlertType.ERROR);
				else
					alert = new Alert("Error", "The image file format is not valid", null, AlertType.ERROR);
				Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
			} catch (PersistenceMechanismException e) {
				Alert alert = null;
				if (e.getCause() instanceof  RecordStoreFullException)
					alert = new Alert( "Error", "The mobile database is full", null, AlertType.ERROR);
				else
					alert = new Alert( "Error", "The mobile database can not update new informations", null, AlertType.ERROR);
				Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
			}
			return true;
		} else if (label.equals("View Favorites")) {
			showMediaList(getCurrentStoreName(), false, true);
			ScreenSingleton.getInstance().setCurrentScreenName(Constants.IMAGELIST_SCREEN);

			return true;
		// &end [includeFavourites]
			
		} else if (label.equals("Save")) {
				System.out
						.println("<* PhotoController.handleCommand() *> Save Photo Label = "
								+ this.screen.getLabelName());
				this.getMedia().setMediaLabel(this.screen.getLabelName());
				try {
					updateMedia(media);
				} catch (InvalidImageDataException e) {
					Alert alert = null;
					if (e instanceof ImagePathNotValidException)
						alert = new Alert("Error", "The path is not valid", null, AlertType.ERROR);
					else
						alert = new Alert("Error", "The image file format is not valid", null, AlertType.ERROR);
					Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
				} catch (PersistenceMechanismException e) {
					Alert alert = new Alert("Error", "The mobile database can not update this photo", null, AlertType.ERROR);
					Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
				}
				return goToPreviousScreen();
		} else if (label.equals("Back")) {
			return goToPreviousScreen();
		} else if (label.equals("Cancel")) {
			return goToPreviousScreen();

		}
		return false;
	}

	
	// &begin [includeMusic]
	private boolean playMusicMedia(String selectedMediaName) {
		InputStream storedMusic = null;
		try {
			MediaData mymedia = getAlbumData().getMediaInfo(selectedMediaName);
			// &begin [includeSorting]
			incrementCountViews(selectedMediaName);
			// &end [includeSorting]
			if (mymedia.getTypeMedia().equals(MediaData.MUSIC)) {
				storedMusic = ((MusicAlbumData) getAlbumData()).getMusicFromRecordStore(getCurrentStoreName(), selectedMediaName);
				PlayMediaScreen playscree = new PlayMediaScreen(midlet,storedMusic, mymedia.getTypeMedia(),this);
				MusicPlayController controller = new MusicPlayController(midlet, getAlbumData(), (AlbumListScreen) getAlbumListScreen(), playscree);
				// &begin [includeCopyMedia]
				controller.setMediaName(selectedMediaName);
				// &end [includeCopyMedia]
				
				this.setNextController(controller);
			}
			return true;
		} catch (ImageNotFoundException e) {
			Alert alert = new Alert( "Error", "The selected item was not found in the mobile device", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
		    return false;
		} 
		catch (PersistenceMechanismException e) {
			Alert alert = new Alert( "Error", "The mobile database can open this item 1", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
			return false;
		}
	
	}
	// &end [includeMusic]
	
	
	// &begin [includeVideo]
	private boolean playVideoMedia(String selectedMediaName) {
		InputStream storedMusic = null;
		try {
			MediaData mymedia = getAlbumData().getMediaInfo(selectedMediaName);
			
			// &begin [includeSorting]
			incrementCountViews(selectedMediaName);
			// &end [includeSorting]
			if (mymedia.getTypeMedia().equals(MediaData.VIDEO)) {
				storedMusic = ((VideoAlbumData) getAlbumData()).getVideoFromRecordStore(getCurrentStoreName(), selectedMediaName);
				PlayVideoScreen playscree = new PlayVideoScreen(midlet,storedMusic, mymedia.getTypeMedia(),this);
				playscree.setVisibleVideo();
				PlayVideoController controller = new PlayVideoController(midlet, getAlbumData(), (AlbumListScreen) getAlbumListScreen(), playscree);
				// &begin [includeCopyMedia]
				controller.setMediaName(selectedMediaName);
				// &end [includeCopyMedia]
				
				this.setNextController(controller);
			}
			return true;
		} catch (ImageNotFoundException e) {
			Alert alert = new Alert( "Error", "The selected item was not found in the mobile device", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
		    return false;
		} 
		catch (PersistenceMechanismException e) {
			Alert alert = new Alert( "Error", "The mobile database can open this item 1", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
			return false;
		}
	}
	// &end [includeVideo]
	
	// &begin [includeAccessControl]
	private void gotoAccessDeniedScreen() {
		AccessDeniedScreen form = new AccessDeniedScreen();
		setCurrentScreen(form);
	}
	// &end [includeAccessControl]

	// &begin [includeSorting]
	private void incrementCountViews(String selectedImageName) {
		try {
			MediaData image = getAlbumData().getMediaInfo(selectedImageName);
			image.increaseNumberOfViews();
			updateMedia(image);
			System.out.println("<* BaseController.handleCommand() *> Image = " + selectedImageName + "; # views = " + image.getNumberOfViews());
		} catch (ImageNotFoundException e) {
			Alert alert = new Alert("Error", "The selected photo was not found in the mobile device", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
		}catch (InvalidImageDataException e) {
			Alert alert = new Alert( "Error", "The image data is not valid", null, AlertType.ERROR);
		    alert.setTimeout(5000);
		} catch (PersistenceMechanismException e) {
			Alert alert = new Alert( "Error", "It was not possible to recovery the selected image", null, AlertType.ERROR);
		    alert.setTimeout(5000);
		}
	}
	// &end [includeSorting]

	void updateMedia(MediaData media) throws InvalidImageDataException, PersistenceMechanismException {
		getAlbumData().updateMediaInfo(media, media);
	}
	
	public String getSelectedMediaName() {
		List selected = (List) Display.getDisplay(midlet).getCurrent();
		if (selected == null)
		    System.out.println("Current List from display is NULL!");
		String name = selected.getString(selected.getSelectedIndex());
		return name;
	}
	
	// &begin [includePhoto]
	public void showImage(String name) {
		Image storedImage = null;
		try {
			storedImage = ((ImageAlbumData)getAlbumData()).getImageFromRecordStore(getCurrentStoreName(), name);
		} catch (ImageNotFoundException e) {
			Alert alert = new Alert( "Error", "The selected photo was not found in the mobile device", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
	        return;
		} catch (PersistenceMechanismException e) {
			Alert alert = new Alert( "Error", "The mobile database can open this photo", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
	        return;
		}		
		
		PhotoViewScreen canv = new PhotoViewScreen(storedImage);
		canv.setCommandListener( this );
		AbstractController nextcontroller = this; 
		// &begin [includeCopyMedia]
		PhotoViewController controller = new PhotoViewController(midlet, getAlbumData(), (AlbumListScreen) getAlbumListScreen(), name);
		controller.setNextController(nextcontroller);
		canv.setCommandListener(controller);
		nextcontroller = controller;
		// &end [includeCopyMedia]
		
		setCurrentScreen(canv);
	}
	// &end [includePhoto]

    private boolean goToPreviousScreen() {
	    System.out.println("<* PhotoController.goToPreviousScreen() *>");
		String currentScreenName = ScreenSingleton.getInstance().getCurrentScreenName();
	    if (currentScreenName.equals(Constants.ALBUMLIST_SCREEN)) {
		    System.out.println("Can't go back here...Should never reach this spot");
		} else if (currentScreenName.equals(Constants.IMAGE_SCREEN)) {
		    showMediaList(getCurrentStoreName(), false, false);
		    ScreenSingleton.getInstance().setCurrentScreenName(Constants.IMAGELIST_SCREEN);
		    return true;
		}
    	else if (currentScreenName.equals(Constants.ADDPHOTOTOALBUM_SCREEN)) {
    		showMediaList(getCurrentStoreName(), false, false);
		    ScreenSingleton.getInstance().setCurrentScreenName(Constants.IMAGELIST_SCREEN);
		    return true;
    	}
	    return false;
    } 

	public void setMedia(MediaData media) {
		this.media = media;
	}

	public MediaData getMedia() {
		return media;
	}

	public void setScreen(NewLabelScreen screen) {
		this.screen = screen;
	}

	public NewLabelScreen getScreen() {
		return screen;
	}
}
