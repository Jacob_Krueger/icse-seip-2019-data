This repository provides the test implementation of a survey on program comprehension and the corresponding results (anonymous) for the paper:

"Effects of Explicit Feature Traceability on Practitioners' Program Comprehension"

submitted to ICSE SEIP 2019.