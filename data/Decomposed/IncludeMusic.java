public class IncludeMusic {

	public static boolean checkLabel(String label) {
		if (label.equals("Play")) {
			String selectedMediaName = getSelectedMediaName();
			return playMusicMedia(selectedMediaName);
		}
		return false;
	}
	
	private static boolean playMusicMedia(String selectedMediaName) {
		InputStream storedMusic = null;
		try {
			MediaData mymedia = getAlbumData().getMediaInfo(selectedMediaName);
			IncludeSorting.incrementCountViews(selectedMediaName);
			if (mymedia.getTypeMedia().equals(MediaData.MUSIC)) {
				storedMusic = ((MusicAlbumData) getAlbumData()).getMusicFromRecordStore(getCurrentStoreName(), selectedMediaName);
				PlayMediaScreen playscree = new PlayMediaScreen(midlet,storedMusic, mymedia.getTypeMedia(),this);
				MusicPlayController controller = new MusicPlayController(midlet, getAlbumData(), (AlbumListScreen) getAlbumListScreen(), playscree);
				IncludeCopyMedia.setMediaName(selectedMediaName);
				
				this.setNextController(controller);
			}
			return true;
		} catch (ImageNotFoundException e) {
			Alert alert = new Alert( "Error", "The selected item was not found in the mobile device", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
		    return false;
		} catch (PersistenceMechanismException e) {
			Alert alert = new Alert( "Error", "The mobile database can open this item 1", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
			return false;
		}
	}
	
	public static void album {
		try {
			if (getAlbumData() instanceof MusicAlbumData){
				getAlbumData().loadMediaDataFromRMS(getCurrentStoreName());
				MediaData mymedia = getAlbumData().getMediaInfo(((AddMediaToAlbum) getCurrentScreen()).getItemName());
				mymedia.setTypeMedia( ((AddMediaToAlbum) getCurrentScreen()).getItemType() );
				getAlbumData().updateMediaInfo(mymedia, mymedia);
			}
		} catch (ImageNotFoundException e) {
			Alert alert = new Alert("Error", "The selected item was not found in the mobile device", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
			return true;
		}
	}
}