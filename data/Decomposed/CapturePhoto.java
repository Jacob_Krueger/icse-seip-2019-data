public class CapturePhoto {

	public static boolean checkLabel(String label) {
		if (label.equals("Capture Photo")) {
			CaptureVideoScreen playscree = new CaptureVideoScreen(midlet, CaptureVideoScreen.CAPTUREPHOTO);
			playscree.setVisibleVideo();
			PhotoViewController controller = new PhotoViewController(midlet, getAlbumData(), (AlbumListScreen) getAlbumListScreen(), "New photo");
			controller.setCpVideoScreen(playscree);
			this.setNextController(controller);
			playscree.setCommandListener(this);
			return true;		
		}
		return false;
	}
}