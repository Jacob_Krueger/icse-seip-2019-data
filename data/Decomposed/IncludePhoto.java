public class IncludePhoto{

	public static boolean checkLabel(String label) {
		if (label.equals("View")) {
			String selectedImageName = getSelectedMediaName();
			showImage(selectedImageName);
			IncludeSorting.incrementCountViews(selectedImageName);
			ScreenSingleton.getInstance().setCurrentScreenName(Constants.IMAGE_SCREEN);
			return true;
		}
		return false;
	}
	
	public void showImage(String name) {
		Image storedImage = null;
		try {
			storedImage = ((ImageAlbumData)getAlbumData()).getImageFromRecordStore(getCurrentStoreName(), name);
		} catch (ImageNotFoundException e) {
			Alert alert = new Alert( "Error", "The selected photo was not found in the mobile device", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
	        return;
		} catch (PersistenceMechanismException e) {
			Alert alert = new Alert( "Error", "The mobile database can open this photo", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
	        return;
		}		
		
		PhotoViewScreen canv = new PhotoViewScreen(storedImage);
		canv.setCommandListener( this );
		AbstractController nextcontroller = this; 
		IncludeCopyMedia.addControler(nextcontroller, canv);
		
		setCurrentScreen(canv);
	}
}