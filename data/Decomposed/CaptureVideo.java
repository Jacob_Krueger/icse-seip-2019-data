public class CaptureVideo {

	public static boolean checkLabel(String label) {
		if (label.equals("Capture Video")) {
			CaptureVideoScreen playscree = new CaptureVideoScreen(midlet, CaptureVideoScreen.CAPTUREVIDEO);
			playscree.setVisibleVideo();
			VideoCaptureController controller = new VideoCaptureController(midlet, getAlbumData(), (AlbumListScreen) getAlbumListScreen(), playscree);
			this.setNextController(controller);
			playscree.setCommandListener(this);
			return true;		
		}
		return false;
	}
}