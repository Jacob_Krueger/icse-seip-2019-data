public class IncludeSorting {
	
	public static boolean checkLabel(String label) {
		if (label.equals("Sort by Views")) {
			showMediaList(getCurrentStoreName(), true, false);
			ScreenSingleton.getInstance().setCurrentScreenName(Constants.IMAGELIST_SCREEN);
			return true;
		}
		return false;
	}
	
	public static void incrementCountViews(String selectedImageName) {
		try {
			MediaData image = getAlbumData().getMediaInfo(selectedImageName);
			image.increaseNumberOfViews();
			updateMedia(image);
			System.out.println("<* BaseController.handleCommand() *> Image = " + selectedImageName + "; # views = " + image.getNumberOfViews());
		} catch (ImageNotFoundException e) {
			Alert alert = new Alert("Error", "The selected photo was not found in the mobile device", null, AlertType.ERROR);
			Display.getDisplay(midlet).setCurrent(alert, Display.getDisplay(midlet).getCurrent());
		}catch (InvalidImageDataException e) {
			Alert alert = new Alert( "Error", "The image data is not valid", null, AlertType.ERROR);
		    alert.setTimeout(5000);
		} catch (PersistenceMechanismException e) {
			Alert alert = new Alert( "Error", "It was not possible to recovery the selected image", null, AlertType.ERROR);
		    alert.setTimeout(5000);
		}
	}
}