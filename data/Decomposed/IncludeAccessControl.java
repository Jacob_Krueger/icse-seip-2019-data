public class IncludeAccessControl {

	public static boolean checkLabel(String label) {
		if (label.equals("Delete Label")) {
			if (!AccessController.hasDeleteRights()) {
				gotoAccessDeniedScreen();
				return true;
			}
		}

		if (label.equals("Add") || label.equals("Edit Label")) {
			if (!AccessController.hasWriteRights()) {
				gotoAccessDeniedScreen();
				return true;
			}
		}
		return false;
	}
	
	private static void gotoAccessDeniedScreen() {
		AccessDeniedScreen form = new AccessDeniedScreen();
		setCurrentScreen(form);
	}
}